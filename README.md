# Primary Goals
1. Build a single page app in Angular 7
2. On launch prompt user for webcam permissions
3. Show a widget with video view
4. Show a button 'Take Snapshot' - clicking the button should take a snapshot (photo) and show it below the video view

# Notes
- You may want to follow [this tutorial](https://www.kirupa.com/html5/accessing_your_webcam_in_html5.htm) for some parts of this assignment

# Stretch Goals
1. Allow recording of video and show recorded video below the live video.